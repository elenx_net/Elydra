#!/bin/bash

LINK="$(readlink --canonicalize "$0")"
ANSIBLE_NOCOWS=1 ansible-playbook "$(dirname "$LINK")/main/elenx.yml" --ask-become-pass --connection=local --inventory=localhost,
