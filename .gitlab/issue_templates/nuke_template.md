## Description
<!-- insert description -->

## Overall Nuke project structure
In future Nuke will be merged with new repository structure, but to make it easier in future we will keep it in separate folder in root project named `nuke`, which firstly will contain `scripts` folder, where all scripts will be situated, later when we start writing roles, they will gradually migrate to their role `files` folder.

In beginning folder structure will look like:
```
Elydra/
-/nuke
--/roles
--/scripts
---/tests
```

The roles will have monolithic structure. They also must have good test and platform coverage. For start we will target next distribution trees:
- Debian (Debian, Ubuntu, Mint)
- Fedora (Fedora, CentOS)

/milestone %"It's time to nuke!" 
/label ~"On hold" ~"Łatwe"
