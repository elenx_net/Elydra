### What  
In scope of this task a role for doing XXX should be created.

### How
Each such role should first do X. 

1. Make Y.
1. Ensure that Z is done.

This role also should may include role XXXjk.

### Roadmap
This role should only change Y, roles Z and T should remain unaffected.

### DEFINITION OF DONE  
After runing role on target host it will have instance of V up and running.

### PROPOSALS
1. Check official documentation for steps to perform XXX.
1. Look for existing roles on Ansible Galaxy. Especially check role XXXyz by N.
1. Try to use modules G and S for task Y.
1. For U some bash scripting may be required.

### Related resources
- [Ansible galaxy](https://galaxy.ansible.com/home)
- [Module index](https://docs.ansible.com/ansible/latest/modules/modules_by_category.html)
- [Ansible issues on GitHub](https://github.com/ansible/ansible/issues)

## Project's wiki  
https://elenx.net/blank/Elydra/wikis/home
  
This roughly describes what should be done in this task. If you have any questions about details or are not sure about something - ask on  `#general` channel. If this description is not clear enough - ask the task author to make it more detailed.
