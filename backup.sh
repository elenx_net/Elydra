#!/bin/bash

ROOT_BACKUP_FOLDER=$( find /media/elentirald -maxdepth 3 -type d -name BACKUP )

if [ $(echo $ROOT_BACKUP_FOLDER | wc --bytes) -eq 1 ]; then
	echo "Could not find BACKUP folder"
	exit;
elif [ $(echo $ROOT_BACKUP_FOLDER | wc --lines) -ne 1 ]; then
	echo "Found more than one BACKUP folder"
	exit;
fi

echo "Using $ROOT_BACKUP_FOLDER as root of backups folder"

NEW_FOLDER_SUFFIX=$( date +%g-%m-%d )
NEW_BACKUP_FOLDER=$( echo "backup_$NEW_FOLDER_SUFFIX" )

BACKUP_FOLDERS=$( ls $ROOT_BACKUP_FOLDER | tr ' ' '\n' | grep --extended-regexp '[0-9]{2}-[0-9]{2}-[0-9]{2}' | tr '\n' ' ' )
echo "Found backup folders: $BACKUP_FOLDERS"

case $( echo $BACKUP_FOLDERS | tr ' ' '\n' | wc --lines ) in
0)
	OVERWRITE=0;
	echo "There is no any backup folder yet, I'll create $NEW_BACKUP_FOLDER"
	;;

1)
	OVERWRITE=0;
	echo "There is only one backup called $BACKUP_FOLDERS, I'll create second one called $NEW_BACKUP_FOLDER"
	;;

2)
	OVERWRITE=1;
	OLD_BACKUP_FOLDER=$( echo $BACKUP_FOLDERS | tr ' ' '\n' | sort | head --lines=1 )
	echo "There are already two backups, I'll overwrite older one that is $OLD_BACKUP_FOLDER"
	;;
esac

if [ $OVERWRITE -eq 1 ]; then
	DESTINATION=$OLD_BACKUP_FOLDER
else
	DESTINATION=$NEW_BACKUP_FOLDER
fi

read --prompt "Execute: rsync /home/elentirald $ROOT_BACKUP_FOLDER/$DESTINATION ? " yN

if [ $yN != "y" ]; then
	echo "OK, not executing it and quitting"
	exit
fi

rsync \
	--verbose \
	--archive \
	--delete \
	--compress \
	--progress \
	/home/elentirald $ROOT_BACKUP_FOLDER/$DESTINATION

read --prompt "Rename $OLD_BACKUP_FOLDER to $NEW_BACKUP_FOLDE ? " yN

if [ $yn != "y" ]; then
	echo "OK, not renaming it and quitting"
	exit
fi

echo "Renaming $OLD_BACKUP_FOLDER to $NEW_BACKUP_FOLDER"
mv $ROOT_BACKUP_FOLDER/$OLD_BACKUP_NAME $ROOT_BACKUP_FOLDER/$NEW_BACKUP_FOLDER

echo "Done"
