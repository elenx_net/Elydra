#!/bin/bash
set -eu
script_directory="$(dirname "$(readlink --canonicalize-missing "$0")")"

pushd "${script_directory}"
ansible-playbook runner.yml "$@"
popd
