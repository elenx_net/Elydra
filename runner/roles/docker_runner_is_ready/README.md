Gitlab Runner
=========

Role Variables
--------------

Same as in runner_is_ready role and:
- container_registry_url
- container_registry_username
- container_registry_password
