Role Name
=========

Role to base on for custom runner configurations

Requirements
------------

Ansible >= 2.9

Role Variables
--------------

Should be provided by user:
- runner_name - used for services and file names
- runner_description - used for runner registration
- runner_tags
- gitlab_url
- gitlab_api_token
- runner_registration_token
