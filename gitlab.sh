#!/bin/bash
TARGET=$1

ANSIBLE_NOCOWS=1 ansible-playbook $(dirname $(readlink -f $0))/main/gitlab.yml --ask-become-pass --inventory=$TARGET,
