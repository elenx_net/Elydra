#!/bin/bash

target=$1

read -p "Path to harlina .jar: " harlina_jar
read -p "Path to elenx-net CA certificate: " harlina_cert
read -p "Harlina database username: " harlina_username
read -s -p "Harlina database password: " harlina_password
echo "OK. Starting playbook..."

ANSIBLE_NOCOWS=1 ansible-playbook $(dirname $(readlink --canonicalize $0))/main/harlina_backend.yml --ask-become-pass --inventory=${target}, --extra-vars "harlina_jar_src_path=${harlina_jar} harlina_database_username=${harlina_username} harlina_database_password=${harlina_password} elenx_cert=${harlina_cert}"
