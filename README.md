# Elydra
We use ansible to automate our infrastructure deploy, this repository contains all our roles.
This readme roughly describes current state of things.

## Project wiki
We have project [wiki](https://elenx.net/blank/Elydra/wikis/home).
It contains study materials, project description, guidelines and will help you with entering this project.
Please make sure you read it before jumping into coding.
If you will stumble upon some problems didn't described there or have some questions - we encourage you to write on slack.

## Project priorities
Any one can pick task that they like. But if you ask about what project currently needs, here is a list:
1. [CICD VPS Migration](https://elenx.net/blank/Elydra/milestones/2)
2. [Main VPS Migration](https://elenx.net/blank/Elydra/milestones/1)
3. [Repository structure refactor](https://elenx.net/blank/Elydra/milestones/5) (needs more tasks)
4. [Global Backup Mechanism](https://elenx.net/blank/Elydra/milestones/4) (needs more tasks)

The milestones 1 and 2 are nearly done.

## Beginners entry point
If you are afraid of Ansible or don't know how and where to start, do not panic!
We have mini-projects for beginners so they can get accustomed to it.

Currently active are:
- [It's time to Nuke!](https://elenx.net/blank/Elydra/milestones/6)

## About repository technical debt
As-is this is monolithic role repository, which is not ideal for larger projects.
There are roles that little or no automatic tests at all and where tested only by hand of developer.

There are roles that are written not according to Ansible standard and some times act more like code the specification. Especially in cases when they are written for multi platform.

There are also many places where code can be written better, like using modules instead of cmd or using yml style when giving module parameters.

Currently CICD is only running linter, no test are run automatically because of some issues with running docker container while being in docker container on runner.

## Roles
There are two main roles - elen and elenx.

Elen is for local environment and is mainly used by @blank when he reinstalls his OS (@elan: "about once i a month")
Elenx is for our servers. At the moment it installs all required packages and configures environment for us.

After we will migrate to new VPS fully project structure will be refined to contain role groups rather then uber roll that uses all other. The main reason is because ElenX requires powerful server to run all the aps it brings.
Konrad Blank [12:29 PM]

## CICD and testing
We also starting to introduce CICD for this repository in form of molecule linter. Later, when we are ready molecule tests will be added to CICD stages, at the moment they are only executed locally.

Most of old roles does not contain any tests. But when you writing new roles you must add tests to them, even thou they won't be executed on CI atm. Later tests will be added to all roles and CI.

## To-Do
Currently repository can be split into three parts: *elen*, *elenX* and *common*.

After creating skeleton we will need to move each role to appropriate place and while moving each role we must apply next:
- Add tests  
- Add sensible defaults
- Re-factor according to our standards.
