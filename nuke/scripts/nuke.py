#!/usr/bin/python3
import argparse
import os
import datetime
import sys
import time as tm

"""
Script using to nuke files or system.

Args:
    time: Delay time to start nuke the system in milliseconds.
    paths: Set of paths using in file only nuke.
    severity: Nuke system type.
    lock: Path to lock file.

Raises
    FileExistsError: when a lockfile is exists.
    ValueError: when you put wrong severity type or paths for FilesOnly Nuke.
"""

parser = argparse.ArgumentParser()

parser.add_argument("-t", "--time", dest="time", default="600000", help="Delay time to start nuke the system in milliseconds")
parser.add_argument("-p", "--paths", dest="paths", help="Set of paths using in file only nuke.")
parser.add_argument("-s", "--severity", dest="severity", required=True, help="Nuke system type")
parser.add_argument("-l", "--lock", dest="lockfile", required=True, help="Path to lock file")
parser.add_argument("-m", "--mute", dest="mute", help="Execute the script without the question prompt")

args = parser.parse_args()
delayTime = args.time


def wholeOS(time=delayTime, lock=args.lockfile):
    print(
        "Attempting to nuke the system. Type: WholeOS. Proceeding in "
        + str(datetime.timedelta(milliseconds=int(time))) + " minutes..."
    )
    tm.sleep(int(time) / 1000)
    print("Nuke activated ! ! !")
    print("sudo rm -rf ./")


def quickOS(time=delayTime, lock=args.lockfile):
    print(
        "Attempting to nuke system. Type: QuickOS. Proceeding in "
        + str(datetime.timedelta(milliseconds=int(time))) + " minutes..."
    )
    tm.sleep(int(time) / 1000)
    print("Nuke activated ! ! !")
    print("sudo rm -rf boot")


def filesOnly(time=delayTime, lock=args.lockfile, paths=args.paths):
    if paths:
        print(
            "Attempting to nuke the system. Type: FilesOnly. Proceeding in "
            + str(datetime.timedelta(milliseconds=int(time))) + " minutes..."
        )
        tm.sleep(int(time) / 1000)
        print("Nuke activated ! ! !")
        print(paths + "/" + "file1.tar deleted")
        print(paths + "/" + "file2.exe deleted")
        print(paths + "/" + "backup.bac deleted")
    else:
        raise ValueError("Paths argument is empty")


def dryRun(time=delayTime, lock=args.lockfile):
    print(
        "Attempting to nuke the system. Type:DryRun. Proceeding in "
        + str(datetime.timedelta(milliseconds=int(time))) + " minutes..."
    )
    print("Lock file is: " + lock)
    tm.sleep(int(time)/1000)
    print("Nuke activated ! ! !")
    print("./directory/file1.tar deleted")
    print("./directory/file2.txt deleted")
    print("./backup/elenxBackup.bac deleted")


def confirmNuke(time=delayTime, lock=args.lockfile, severity=args.severity, mute=args.mute):
    if mute == '1':
        return True

    question = f"""
        Severity: {severity}
        Lockfile: {lock}
        Time: {time} milliseconds 
        Are you sure you want to perform a nuke on this system?
        Type yes to confirm the operation.
    """

    reply = str(input(question)).lower().strip()

    return reply == 'yes'


severityType = dict(
    dryrun=dryRun,
    quickos=quickOS,
    filesonly=filesOnly,
    wholeos=wholeOS,
)


def nuke(severity, lock):
    if os.path.exists(lock):
        raise FileExistsError("The lock file has already been created!")

    if not confirmNuke():
        print("Confirmation cancelled, the script will not be executed.")
        return 0

    lock_file = open(lock, 'w')
    lock_file.writelines(str(os.getpid()))
    lock_file.close()

    if severity not in severityType.keys():
        raise ValueError("Wrong severity type. Check possible severity using --help")

    try:
        severityType.get(severity)()
    finally:
        os.remove(lock)


if __name__ == '__main__':
    nuke(severity=args.severity, lock=args.lockfile)
    sys.exit(0)
