Gitlab Runner
=========

Role Variables
--------------

Variable descriptions || examples:
- bind_domain: elenx.net
- bind_name_server_ip: IP of this dns
- bind_domain_ip: IP of elenx.net
- bind_id_addr_arpa_ip - 3 first IP octets reversed of {{ bind_domain_ip }}
- bind_domain_last_octet - last ip octet of {{ bind_domain_ip }}
- bind_mail_ip: IP of mail.elenx.net
- bind_ftp_ip: IP of ftp.elenx.net
- bind_container_registry_ip - IP of registry.elenx.net
