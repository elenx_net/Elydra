#!/bin/bash

if [ "$1" = "-h" ] || [ "$1" = "--help" ] || [ "$#" -ne  4 ]; then
        echo "backup.sh - runs duplicity backup, use:"
        echo "    backup.sh [backup_mail] [backup_source] [backup_location] [backup_retention_time]"
        echo "All arguments are written in format supported by duplicity"
        exit 0
fi

BACKUP_MAIL=$1
BACKUP_SOURCE=$2
BACKUP_LOCATION=$3
BACKUP_RETENTION_TIME=$4


echo "--> Backing up files..."
duplicity --encrypt-key $BACKUP_MAIL $BACKUP_SOURCE $BACKUP_LOCATION

echo "--> Cleaning up old backup files..."
duplicity remove-older-than $BACKUP_RETENTION_TIME $BACKUP_LOCATION
