#!/bin/bash

BACKUP_MAIL=$1
BACKUP_LOCATION=$3
RESTORED_FILE_DESTINATION=$2


echo "--> Restoring files..."
duplicity --encrypt-key $BACKUP_MAIL --file-to-restore $RESTORE_FILE_NAME $BACKUP_LOCATION $RESTORED_FILE_DESTINATION
