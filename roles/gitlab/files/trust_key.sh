#!/usr/bin/expect

set KEY_TO_TRUST $argv

set timeout 10

spawn /usr/bin/gpg --edit-key $KEY_TO_TRUST 0 trust quit

expect "Your decision? " { send "5\n" }
expect "Do you really want to set this key to ultimate trust? (y/N) " { send "y\n" }

interact
