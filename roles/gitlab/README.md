GitLab
=========

Installs clean GitLab instance. And setups encrypted backups.

Requirements
--------------

Backup security
1. Generating gpg key:
`gpg --full-generate-key`
1. Exporting keys for backup:
```bash
gpg --export-secret-keys --armor user@mail > ~/.gpg/backup_user.private.gpg-key
gpg --export --armor user@mail > ~/.gpg/backup_user.public.gpg-key
```

Role Variables
--------------

- `gitlab_backup_path`: Path where where GitLab stores it's backups, when doing backup gitlab-rake. This does not mean that GitLab will create backups automatically.
- `gitlab_external_backup_location`: location where both meta and data will be stored
- `gitlab_external_backup_location`: is specified using [duplicity url format](http://duplicity.nongnu.org/duplicity.1.html#sect7)
- `gitlab_backup_mail`: when we ask duplicity to encrypt with key we identify it with mail address

Dependencies
------------

Requires roles from `requirements.yml` beeing present on controlling host.
