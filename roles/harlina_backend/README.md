Harlina Backend
===============

Sets up Harlina Backend as systemd service. Database is not included. 
`.jar` file, ElenX.net CA certificate, and database credentials need to be provided.
Application runs on designated `harlina` user and makes use of chrome (webdriver) and Xvfb (fake display for chrome)
