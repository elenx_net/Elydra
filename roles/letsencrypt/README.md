Letsencrypt
=========

Sets up Geerlingguy letsencrypt role.

Please refer to the [original playbook docs](https://github.com/geerlingguy/ansible-role-certbot).

After creation, the certificates and keys are available at:

```markdown
/etc/letsencrypt/live/myexampledomain.com/
```

Role Variables
--------------

- `certbot_create_standalone_stop_services`: For example nginx, apache, etc. Refers to services listening on port 80.


Dependencies
------------

- geerlingguy.certbot
