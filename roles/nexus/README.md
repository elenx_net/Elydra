Nexus
=========

Installs Nexus repository on the target machine.

Supports installation from backup.

Please refer to the documentation from the [official role page](https://github.com/ansible-ThoTeam/nexus3-oss) 

Dependencies
------------

- ansible-ThoTeam.nexus3-oss
